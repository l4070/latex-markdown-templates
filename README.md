# LaTeX Markdown Templates

The aim of this project is using LaTeX templates to be loaded via the ekyll header of markdown. This mechanism shall simplify the use of LaTeX for unexperiences users.

By writing template files experienced LaTeX users can change the layout or the document structure. The content itself can be edited collaborative either via git, the git server own IDE or any online pad hosting service (e.g. hedgedoc). The build process is configured to run via continous integration.

Currently there example projects from a LaTeX workshop given at INFOS2021. those can be found in the Group „Schule“.

„Kollaborative Dokumentenerstellung mit LaTeX” https://dl.gi.de/handle/20.500.12116/36970

Marei Peischl <marei@peitex.de> & André Hilbig <hilbig@uni-wuppertal.de>

